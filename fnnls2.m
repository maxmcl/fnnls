function [x, r] = fnnls2(A, b, tol)
%% FNNLS2
% Initialize variables
n = size(A, 2);
if nargin < 3
    tol = 10 * eps * norm(A,1) * n;
end

% Every variable is at bound initially
working = false(n, 1); % ACTIVE = ~ WORKING
% x0 set at 0
nZeros = zeros(n, 1);
x = nZeros;
% Initial resdiual
r = b - A*x;
% KKT conditions
kkt = A' * r;
kktZ = nZeros;
xU = nZeros;

% Set up iteration criterion
iter = 1;
itmax = 3 * n;

%% Outer loop
% Put variables into set to hold positive coefficients
while any(~working) && any(kkt(~working) > tol)
    % Finding position of maximal residual
    kktZ(working) = -Inf;
    kktZ(~working) = kkt(~working);
    [~, maxInd] = max(kktZ);
    
    % Index maxInd enters working set
    working(maxInd) = true;
    
    xU(working)= A(:, working) \ b;
    
    %% Inner loop
    % Check if working variables violate the bound / are now in the active
    % set
    while any(xU(working) <= 0) && iter < itmax
        % Getting working variables that violate the bound
        violate = (xU <= 0) & working;
        
        % Computing step to keep x positive
        % x + alpha (xU - x) >= 0 <=> alpha = - x / (xU - x)
        alpha = min(x(violate) ./ (x(violate) - xU(violate)));
        x = x + alpha * (xU - x);
        
        % Check if working variables entered the active set
        newActive= ((abs(x) < tol) & working);
        working(newActive) = false;
        
        xU(working)= A(:, working) \ b;
        xU(newActive) = 0;
        
        iter = iter + 1;
    end
    % Updating current point and residual
    x = xU;
    r = b - A*x;
    kkt = A' * r;
end
end
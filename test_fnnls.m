%% Testing FNNLS on the problem
% min_x { 1/2 * || A*x - b ||^2 : x >= 0 }
clc;
clear all;
close all;

addpath('~/Masters/Krylov.m');
addpath('~/Masters/Spot');

%% Generate problem size
m = 500;
n = 500;
% m = randi(5000, 1, 1);
% n = randi(100, 1, 1);

%% Generate random 1/2 * || A*x - b ||^2
A = randi(1e3, m, n) - randi(1e3, m, n);
b = -randi(50, m, 1) - randi(50, m, 1);

%% Solve using lsqnonneg
tic;
tol = 10 * eps * norm(A, 1) * length(A);
opt = optimset('TolX', tol);
x1 = lsqnonneg(A, b, opt);
t1 = toc;
fprintf('lsqnonneg!\n');
%% Solve using FNNLS
tic;
AtA = A'*A;
Atb = A'*b;
x2 = fnnls(AtA, Atb);
t2 = toc;
fprintf('fnnls!\n');
%% Solve using FNNLSKRYLOV
solver = 'pcg';
tic;
AtAop = opFunction(m, n, @(x, mode) AtA*x);
x3 = fnnlsKrylov(AtAop, Atb, tol, solver);
t3 = toc;
fprintf('fnnlsKrylov!\n');
%% Checking solutions
t1
t2
t3
norm(x1 - x2)
norm(x1 - x3)
function [x, r] = fnnls(AtA, Atb, tol)
%% FNNLS
% Initialize variables
n = length(AtA); % AtA is square
if nargin < 3
    tol = 10 * eps * norm(AtA, 1) * n;
end

% Every variable is at bound initially
working = false(n, 1); % ACTIVE = ~ WORKING
% x0 set at 0
x = zeros(n, 1);
xU = x;
% Initial resdiual
r = Atb - AtA*x;

% Set up iteration criterion
iter = 1;
itmax = 3 * n;

%% Outer loop
% Put variables into set to hold positive coefficients
while any(~working) && any(r(~working) > tol)
    % Finding position of maximal residual
    [~, maxInd] = max(r(~working));
    temp = find(~working);
    maxInd = temp(maxInd);
    
    % Index maxInd enters working set
    working(maxInd) = true;
    
    xU(working) = AtA(working, working) \ Atb(working);
    %% Inner loop
    % Check if working variables violate the bound / are now in the active
    % set
    while any(xU(working) <= 0) && iter < itmax
        % Getting working variables that violate the bound
        violate = (xU <= 0) & working;
        
        % Computing step to keep x positive
        % x + alpha (xU - x) >= 0 <=> alpha = - x / (xU - x)
        alpha = min(x(violate) ./ (x(violate) - xU(violate)));
        x = x + alpha * (xU - x);
        
        % Check if working variables entered the active set
        nowActive = (abs(x) < tol) & working;
        working(nowActive) = false;
        
        xU(working) = AtA(working, working) \ Atb(working);
        % Working might've changed, new variables may be at bound
        xU(nowActive) = 0;
        
        iter = iter + 1;
    end
    % Updating current point and residual
    x = xU;
    r = Atb - AtA*x;
end
end
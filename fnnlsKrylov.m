function [x, r] = fnnlsKrylov(AtA, Atb, tol, solver)
%% FNNLSKRYLOV
% Initialize variables
n = length(AtA); % AtA is square
if nargin < 3
    tol = 10 * eps * norm(AtA, 1) * n;
    solver = 'pcg';
end
if nargin < 4
    solver = 'pcg';
end
solveTol = 1e-12;
% Every variable is at bound initially
working = false(n, 1); % ACTIVE = ~ WORKING
% x0 set at 0
x = zeros(n, 1);
xU = x;
% Initial resdiual
r = Atb - AtA*x;

% Set up iteration criterion
iter = 1;
itmax = 3 * n;

%% Outer loop
% Put variables into set to hold positive coefficients
while any(~working) && any(r(~working) > tol)
    % Finding position of maximal residual
    [~, maxInd] = max(r(~working));
    temp = find(~working);
    maxInd = temp(maxInd);
    
    % Index maxInd enters working set
    working(maxInd) = true;
    
    xU(working) = solveUnc(AtA, Atb, working, solver, solveTol);
    %% Inner loop
    % Check if working variables violate the bound / are now in the active
    % set
    while any(xU(working) <= 0) && iter < itmax
        % Getting working variables that violate the bound
        violate = (xU <= 0) & working;
        
        % Computing step to keep x positive
        % x + alpha (xU - x) >= 0 <=> alpha = - x / (xU - x)
        alpha = min(x(violate) ./ (x(violate) - xU(violate)));
        x = x + alpha * (xU - x);
        
        % Check if working variables entered the active set
        nowActive = (abs(x) < tol) & working;
        working(nowActive) = false;
        
        xU(working) = solveUnc(AtA, Atb, working, solver, solveTol);
        % Working might've changed, new variables may be at bound
        xU(nowActive) = 0;
        
        iter = iter + 1;
    end
    % Updating current point and residual
    x = xU;
    r = Atb - AtA*x;
end
end

function x = solveUnc(AtA, Atb, working, solver, tol)
%% Calling a Krylov solver to solve AtA\Atb on the working set
% xU(working) = AtA(working, working) \ Atb(working);
switch solver
    case 'pcg'
        [x, ~] = pcg(AtA(working, working), Atb(working), tol);
    case 'minres'
        [x, ~] = minres(AtA(working, working), Atb(working), tol);
    case 'minres_spot'
        krylOpts.rtol = tol;
        krylOpts.etol = tol;
        krylOpts.shift = 0;
        krylOpts.show = false;
        krylOpts.check = false;
        x = minres_spot(AtA(working, working), Atb(working), krylOpts);
    case 'lsqr'
        krylOpts.rtol = tol;
        krylOpts.etol = tol;
        krylOpts.shift = 0;
        krylOpts.show = false;
        krylOpts.check = false;
        x = lsqr_spot(AtA(working, working), Atb(working), krylOpts);
    case 'lsmr'
        krylOpts.rtol = tol;
        krylOpts.etol = tol;
        krylOpts.shift = 0;
        krylOpts.show = false;
        krylOpts.check = false;
        x = lsmr_spot(AtA(working, working), Atb(working), krylOpts);
    otherwise
        error('Unrecognized linear solver');
end
end
